# Copyright (c) 2017, Cyril Roelandt
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
# IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
# TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
# TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
import argparse
import calendar
import datetime

import daemonize
from flask import Flask, render_template

from poorer import db


app = Flask(__name__)
expense_directory = None


def _get_first_last_day_month(year, month):
    """Return the first and last day of a given month as datetime.dates."""
    _, l = calendar.monthrange(year, month)
    return (datetime.date(year, month, 1),
            datetime.date(year, month, l))


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/<int:year>', strict_slashes=False)
def stats_per_year(year):
    min_time = datetime.date(year, 1, 1)
    max_time = datetime.date(year, 12, 31)
    spendings = db.get_spendings_by_recipients(min_time=min_time,
                                               max_time=max_time)
    spendings_category = db.get_spendings_by_categories(min_time=min_time,
                                                        max_time=max_time)
    earnings = db.get_earnings_by_source(min_time=min_time,
                                         max_time=max_time)
    return render_template('spendings.html',
                           year=year,
                           earnings=earnings,
                           spendings=spendings,
                           spendings_category=spendings_category)


@app.route('/<int:year>/<int:month>', strict_slashes=False)
def stats_per_month(year, month):
    min_time, max_time = _get_first_last_day_month(year, month)
    spendings = db.get_spendings_by_recipients(min_time=min_time,
                                               max_time=max_time)
    spendings_category = db.get_spendings_by_categories(min_time=min_time,
                                                        max_time=max_time)
    earnings = db.get_earnings_by_source(min_time=min_time,
                                         max_time=max_time)
    return render_template('spendings.html',
                           year=year,
                           month=month,
                           earnings=earnings,
                           spendings=spendings,
                           spendings_category=spendings_category)


@app.route('/to', strict_slashes=False)
def to():
    recipients = db.get_recipients()
    return render_template('to.html', recipients=recipients)


@app.route('/to/<recipient>', strict_slashes=False)
def to_recipient(recipient):
    spendings = db.get_spendings_for_recipient(recipient)
    return render_template('to_recipient.html', spendings=spendings)


@app.route('/refresh')
def refresh_db():
    db.clear()
    db.yaml_to_db(expense_directory)
    return ''


def create_parser():
    description = "Proletarian-Oriented Overview of Ridiculous Expense Reports"
    parser = argparse.ArgumentParser(description=description,
                                     allow_abbrev=False)
    parser.add_argument('-d', '--daemon', action='store_true',
                        help='Run as daemon')
    parser.add_argument('-p', '--port', type=int, default=5000,
                        help='Port to listen to')
    parser.add_argument('--locale', type=str, default="fr",
                        help='Currency symbol/conventions to use '
                             '(default: fr). Supported currencies are: '
                             'be-nl, bg, chs, cs, da-dk, de-ch, de, en-au, '
                             'en-gb, en-za, es-es, es, et, fi, fr-ca, fr-ch, '
                             'fr, hu, it, ja, lv, nl-nl, no, pl, pt-br, '
                             'pt-pt, ru, ru-ua, sk, sl, th, tr, uk-ua. See '
                             'http://numeraljs.com/#locales for more info. '
                             'This does *not* change the locale of the '
                             'application, only the way numbers are '
                             'represented.')
    parser.add_argument('directory', help='expense directory')
    return parser


def main():
    parser = create_parser()
    args = parser.parse_args()

    global expense_directory
    expense_directory = args.directory

    db.yaml_to_db(expense_directory)

    @app.context_processor
    def locale_processor():
        return {
            'locale': args.locale,
        }

    if args.daemon:
        daemon = daemonize.Daemonize(app="poorer", pid="/tmp/poorer.pid",
                                     action=lambda: app.run(port=args.port))
        daemon.start()
    else:
        app.run(port=args.port)


if __name__ == '__main__':
    main()
